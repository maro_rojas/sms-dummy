﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dummy.Pruebas
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void ServicioDisponible_Prueba()
		{
			var resultado = false;
			var servicio = new Servicio.Dummy.Service1Client();
			try
			{
				resultado = servicio.ServicioDisponible();

			}
			finally
			{
				servicio.Close();
			}
			Assert.IsTrue(resultado);
		}
	}
}
