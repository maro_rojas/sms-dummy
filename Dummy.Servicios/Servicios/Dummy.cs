﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Dummy.Servicios
{
	[ServiceBehavior]
	public class Dummy : IService1
	{
		[OperationBehavior]
		public bool ServicioDisponible()
		{
			return true;
		}
	}
}
