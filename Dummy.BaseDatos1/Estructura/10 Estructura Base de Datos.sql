/*
 * ER/Studio Data Architect 10.0 SQL Code Generation
 * Project :      Corvus Model.dm1
 *
 * Date Created : Tuesday, September 01, 2015 09:27:08
 * Target DBMS : Microsoft SQL Server 2012
 */

USE CorvusSMS
go
/* 
 * TABLE: [Client] 
 */

CREATE TABLE [Client](
    [id]                int               IDENTITY(1,1),
    [allow_ip]          varchar(30)       NOT NULL,
    [default_endpoint]  varchar(40)       NULL,
    [name]              varchar(30)       NULL,
    [token]             varbinary(max)    NULL,
    [allow-answer]      varchar(30)       NULL
)
go



IF OBJECT_ID('Client') IS NOT NULL
    PRINT '<<< CREATED TABLE Client >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Client >>>'
go


/* 
 * TABLE: [Client] 
 */

ALTER TABLE [Client] ADD 
    CONSTRAINT [PK_ID_CLIENTE] PRIMARY KEY CLUSTERED ([id])
go

IF OBJECT_ID('Client') IS NOT NULL
    PRINT '<<< CREATED TABLE Client >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Client >>>'
go

/* 
 * TABLE: [Device] 
 */

CREATE TABLE [Device](
    [id]             int            IDENTITY(1,1),
    [model]          varchar(30)    NULL,
    [phone_number]   varchar(10)    NULL,
    [port]           varchar(18)    NULL,
    [serial_number]  varchar(40)    NULL,
    [pin]            varchar(10)    NULL,
    [alias]          varchar(30)    NULL,
    [brand]          varchar(40)    NULL
)
go



IF OBJECT_ID('Device') IS NOT NULL
    PRINT '<<< CREATED TABLE Device >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Device >>>'
go


/* 
 * TABLE: [Device] 
 */

ALTER TABLE [Device] ADD 
    CONSTRAINT [PK_ID_DEVICE] PRIMARY KEY CLUSTERED ([id])
go

IF OBJECT_ID('Device') IS NOT NULL
    PRINT '<<< CREATED TABLE Device >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Device >>>'
go

/* 
 * TABLE: [Filter] 
 */

CREATE TABLE [Filter](
    [id]             int            IDENTITY(1,1),
    [id_client]      int            NOT NULL,
    [key_filter]     varchar(50)    NULL,
    [endpoint]       varchar(50)    NULL,
    [endpoint_type]  varchar(50)    NULL
)
go



IF OBJECT_ID('Filter') IS NOT NULL
    PRINT '<<< CREATED TABLE Filter >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Filter >>>'
go


/* 
 * TABLE: [Filter] 
 */

ALTER TABLE [Filter] ADD 
    CONSTRAINT [PK_ID_FILTER] PRIMARY KEY CLUSTERED ([id])
go

IF OBJECT_ID('Filter') IS NOT NULL
    PRINT '<<< CREATED TABLE Filter >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Filter >>>'
go

/* 
 * TABLE: [Parameter] 
 */

CREATE TABLE [Parameter](
    [id]               int            IDENTITY(1,1),
    [id_filter]        int            NOT NULL,
    [name]             varchar(50)    NULL,
    [type]             varchar(50)    NULL,
    [order_parameter]  int            NULL
)
go



IF OBJECT_ID('Parameter') IS NOT NULL
    PRINT '<<< CREATED TABLE Parameter >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Parameter >>>'
go


/* 
 * TABLE: [Parameter] 
 */

ALTER TABLE [Parameter] ADD 
    CONSTRAINT [PK_ID_PARAMETER] PRIMARY KEY CLUSTERED ([id])
go

IF OBJECT_ID('Parameter') IS NOT NULL
    PRINT '<<< CREATED TABLE Parameter >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Parameter >>>'
go

/* 
 * TABLE: [Received_Message] 
 */

CREATE TABLE [Received_Message](
    [id]            int             IDENTITY(1,1),
    [id_device]     int             NOT NULL,
    [id_client]     int             NOT NULL,
    [phone_number]  varchar(8)      NULL,
    [content_sms]   varchar(160)    NULL
)
go



IF OBJECT_ID('Received_Message') IS NOT NULL
    PRINT '<<< CREATED TABLE Received_Message >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Received_Message >>>'
go


/* 
 * TABLE: [Received_Message] 
 */

ALTER TABLE [Received_Message] ADD 
    CONSTRAINT [PK_ID_RECEIVED_MESSAGE] PRIMARY KEY CLUSTERED ([id])
go

IF OBJECT_ID('Received_Message') IS NOT NULL
    PRINT '<<< CREATED TABLE Received_Message >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Received_Message >>>'
go

/* 
 * TABLE: [Rol] 
 */

CREATE TABLE [Rol](
    [id]           int            IDENTITY(1,1),
    [description]  varchar(50)    NULL,
    [name]         varchar(50)    NULL
)
go



IF OBJECT_ID('Rol') IS NOT NULL
    PRINT '<<< CREATED TABLE Rol >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Rol >>>'
go


/* 
 * TABLE: [Rol] 
 */

ALTER TABLE [Rol] ADD 
    CONSTRAINT [PK_ID_ROL] PRIMARY KEY CLUSTERED ([id])
go

IF OBJECT_ID('Rol') IS NOT NULL
    PRINT '<<< CREATED TABLE Rol >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Rol >>>'
go

/* 
 * TABLE: [Sent_Message] 
 */

CREATE TABLE [Sent_Message](
    [id]            int             IDENTITY(1,1),
    [id_device]     int             NOT NULL,
    [id_client]     int             NOT NULL,
    [content]       varchar(150)    NULL,
    [phone_number]  varchar(10)     NULL
)
go



IF OBJECT_ID('Sent_Message') IS NOT NULL
    PRINT '<<< CREATED TABLE Sent_Message >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Sent_Message >>>'
go


/* 
 * TABLE: [Sent_Message] 
 */

ALTER TABLE [Sent_Message] ADD 
    CONSTRAINT [PK_ID_SENT_MESSAGE] PRIMARY KEY CLUSTERED ([id])
go

IF OBJECT_ID('Sent_Message') IS NOT NULL
    PRINT '<<< CREATED TABLE Sent_Message >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Sent_Message >>>'
go

/* 
 * TABLE: [SentMessageDummy] 
 */

CREATE TABLE [SentMessageDummy](
    [id]            int            NOT NULL,
    [content]       varchar(30)    NULL,
    [phone_number]  varchar(50)    NULL
)
go



IF OBJECT_ID('SentMessageDummy') IS NOT NULL
    PRINT '<<< CREATED TABLE SentMessageDummy >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE SentMessageDummy >>>'
go


/* 
 * TABLE: [SentMessageDummy] 
 */

ALTER TABLE [SentMessageDummy] ADD 
    CONSTRAINT [PK_ID_SENT_MESSAGE_DUMMY] PRIMARY KEY CLUSTERED ([id])
go

IF OBJECT_ID('SentMessageDummy') IS NOT NULL
    PRINT '<<< CREATED TABLE SentMessageDummy >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE SentMessageDummy >>>'
go

/* 
 * TABLE: [User] 
 */

CREATE TABLE [User](
    [id]              int            IDENTITY(1,1),
    [mail]            varchar(30)    NULL,
    [password]        varchar(30)    NULL,
    [username]        varchar(50)    NULL,
    [change_request]  bit            NULL
)
go



IF OBJECT_ID('User') IS NOT NULL
    PRINT '<<< CREATED TABLE User >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE User >>>'
go


/* 
 * TABLE: [User] 
 */

ALTER TABLE [User] ADD 
    CONSTRAINT [PK_ID_USER] PRIMARY KEY CLUSTERED ([id])
go

IF OBJECT_ID('User') IS NOT NULL
    PRINT '<<< CREATED TABLE User >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE User >>>'
go

/* 
 * TABLE: [User_Rol] 
 */

CREATE TABLE [User_Rol](
    [id]       int    NOT NULL,
    [id_user]  int    NOT NULL,
    [id_rol]   int    NOT NULL
)
go



IF OBJECT_ID('User_Rol') IS NOT NULL
    PRINT '<<< CREATED TABLE User_Rol >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE User_Rol >>>'
go


/* 
 * TABLE: [User_Rol] 
 */

ALTER TABLE [User_Rol] ADD 
    CONSTRAINT [PK_ID_USER_ROL] PRIMARY KEY CLUSTERED ([id])
go

IF OBJECT_ID('User_Rol') IS NOT NULL
    PRINT '<<< CREATED TABLE User_Rol >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE User_Rol >>>'
go

/* 
 * TABLE: [Filter] 
 */

ALTER TABLE [Filter] ADD 
    FOREIGN KEY ([id_client])
    REFERENCES [Client]([id])
go


/* 
 * TABLE: [Parameter] 
 */

ALTER TABLE [Parameter] ADD 
    FOREIGN KEY ([id_filter])
    REFERENCES [Filter]([id])
go


/* 
 * TABLE: [Received_Message] 
 */

ALTER TABLE [Received_Message] ADD 
    FOREIGN KEY ([id_device])
    REFERENCES [Device]([id])
go

ALTER TABLE [Received_Message] ADD 
    FOREIGN KEY ([id_client])
    REFERENCES [Client]([id])
go


/* 
 * TABLE: [Sent_Message] 
 */

ALTER TABLE [Sent_Message] ADD 
    FOREIGN KEY ([id_client])
    REFERENCES [Client]([id])
go

ALTER TABLE [Sent_Message] ADD 
    FOREIGN KEY ([id_device])
    REFERENCES [Device]([id])
go


/* 
 * TABLE: [User_Rol] 
 */

ALTER TABLE [User_Rol] ADD 
    FOREIGN KEY ([id_user])
    REFERENCES [User]([id])
go

ALTER TABLE [User_Rol] ADD 
    FOREIGN KEY ([id_rol])
    REFERENCES [Rol]([id])
go


