USE CorvusSMS
go

IF OBJECT_ID('[Parameter]') IS NOT NULL
BEGIN
    DROP TABLE [Parameter]
    PRINT '<<< DROPPED TABLE [Parameter] >>>'
END
go

IF OBJECT_ID('[Filter]') IS NOT NULL
BEGIN
    DROP TABLE [Filter]
    PRINT '<<< DROPPED TABLE [Filter] >>>'
END
go


IF OBJECT_ID('[Received_Message]') IS NOT NULL
BEGIN
    DROP TABLE [Received_Message]
    PRINT '<<< DROPPED TABLE [Received_Message] >>>'
END
go

IF OBJECT_ID('[Sent_Message]') IS NOT NULL
BEGIN
    DROP TABLE [Sent_Message]
    PRINT '<<< DROPPED TABLE [Sent_Message] >>>'
END
go

IF OBJECT_ID('[Device]') IS NOT NULL
BEGIN
    DROP TABLE [Device]
    PRINT '<<< DROPPED TABLE [Device] >>>'
END
go


IF OBJECT_ID('[User_Rol]') IS NOT NULL
BEGIN
    DROP TABLE [User_Rol]
    PRINT '<<< DROPPED TABLE [User_Rol] >>>'
END
go

IF OBJECT_ID('[Rol]') IS NOT NULL
BEGIN
    DROP TABLE [Rol]
    PRINT '<<< DROPPED TABLE [Rol] >>>'
END
go


IF OBJECT_ID('[SentMessageDummy]') IS NOT NULL
BEGIN
    DROP TABLE [SentMessageDummy]
    PRINT '<<< DROPPED TABLE [SentMessageDummy] >>>'
END
go

IF OBJECT_ID('[User]') IS NOT NULL
BEGIN
    DROP TABLE [User]
    PRINT '<<< DROPPED TABLE [User] >>>'
END
go


IF OBJECT_ID('[Client]') IS NOT NULL
BEGIN
    DROP TABLE [Client]
    PRINT '<<< DROPPED TABLE [Client] >>>'
END
go

